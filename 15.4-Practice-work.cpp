﻿#include <iostream>
using namespace std;

void findSpecNums(int limit, bool isOdd)
{
    for (int i = 0; i <= limit; ++i)
    {
        if (isOdd == 1)
        {
            if (i % 2 == 0)
            {
                cout << i << endl;
            }
        }
        if (isOdd == 0)
        {
            if (i % 2 != 0)
            {
                cout << i << endl;
            }
        }
    }
}

int main()
{
    int limit;
    bool isOdd;

    cout << "enter a limit number: ";
    cin >> limit;

    cout << "odd = 1" << endl;
    cout << "even = 0" << endl;
    cout << "enter odd/even: ";
    cin >> isOdd;
    


    findSpecNums(limit, isOdd);
}
